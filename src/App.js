import React, { Component } from 'react';
import logo from './hspc-logo-md.png';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <img src={logo} alt="logo" />
        </header>
        <h2 className="App-intro">
          Temporarily down for maintenance. Be up in a jiffy.
        </h2>
      </div>
    );
  }
}

export default App;
